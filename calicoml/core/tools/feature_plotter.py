# -*- coding: utf-8 -*-

"""\
Copyright (c) 2015-2018, MGH Computational Pathology

"""

import pandas as pd
import numpy as np

from argparse import ArgumentParser
from calicoml.core.problem import Problem
from calicoml.core.reporting import ReportRenderer, CorrelationReport
from calicoml.core.utils import is_numeric


def main(args=None):
    """

    :param args: parameters passed as command line arguments
    :return: a report is generated, composed of a pdf containing plotted graphs and a text
            file containing features sorted by AUC values.

    """
    parser = ArgumentParser(description="Plots features against an outcome feature.")
    parser.add_argument('input_file', help='Location of the input file')
    parser.add_argument('output', help='Where to save the output')
    parser.add_argument('--delimiter', default='\t', help='Delimiter that seperates the data')
    parser.add_argument('outcome_column', help='The outcome feature')
    parser.add_argument('positive_outcome', help='A positive outcome in the outcome feature')
    parser.add_argument('numeric_features', nargs='+', help='List of continuous variables in the data')
    parsed_args = parser.parse_args(args)

    df = pd.read_csv(parsed_args.input_file, sep=parsed_args.delimiter)
    # Clean up the columns we expect to be numeric
    # TODO: this should be a standard thing that vectorize() does
    for feat in parsed_args.numeric_features:
        df[feat] = np.asarray([float('nan') if not is_numeric(x) else x for x in df[feat]], dtype=np.float64)

    problem = Problem(df, parsed_args.numeric_features, parsed_args.outcome_column, parsed_args.positive_outcome)
    report = CorrelationReport(ReportRenderer(parsed_args.output))
    return report.generate(problem)


if __name__ == "__main__":
    main()

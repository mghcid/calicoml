# -*- coding: utf-8 -*-

"""\
Copyright (c) 2015-2018, MGH Computational Pathology

"""

from __future__ import print_function

from collections import defaultdict, Counter
from calicoml.core.metrics import ROC, accuracy_from_confusion_matrix
from calicoml.core.utils import assert_rows_are_concordant, partially_reorder_columns
from calicoml.core.problem import Problem

import numpy as np
import pandas as pd

import os
from rpy2.robjects import pandas2ri
pandas2ri.activate()
from rpy2 import robjects

import rpy2
from rpy2.robjects.lib.ggplot2 import ggplot2, theme_bw, theme_set
from rpy2.robjects.packages import importr
from sklearn.metrics import roc_auc_score


class ReportRenderer(object):
    """\
    Renders reports. Currently simply writes them to files in the specified directly, but in the
    future can be extended to support databases, a web UI etc.

    """
    def __init__(self, directory, create_directory=True, tsv=True, xls=False,
                 width=14, height=8):
        """\

        :param directory: where to store the reports

        """
        self.tables, self.plots = {}, {}

        if isinstance(directory, (list, tuple)):
            directory = os.path.join(*directory)

        self.directory = directory
        self.tsv = tsv
        self.xls = xls
        self.width, self.height = width, height

        if create_directory and not os.path.exists(directory):
            os.mkdir(directory)

    def add_table(self, name, df, index=False, index_col=None):
        """\
        Adds a table to the report.

        :param name: name of the table
        :param df: a pandas DataFrame with table contents
        :param index: whether to save the table's index. Default: False
        :param index_col: column name to use for the index
        :return: None

        """
        self.tables[name] = df

        if self.tsv:
            df.to_csv(os.path.join(self.directory, '{}.txt'.format(name)), sep='\t',
                      index=index, index_label=index_col)
        if self.xls:
            df.to_excel(os.path.join(self.directory, '{}.xlsx'.format(name)), index=index, index_label=index_col)

    def add_plot(self, name, plots):
        """\
        Adds a plot to the report.

        :param name: name of the plot
        :param plots: one or more ggplot instances
        :return: None

        """
        self.plots[name] = plots

        robjects.r['pdf'](os.path.join(self.directory, '{}.pdf'.format(name)), width=self.width, height=self.height)
        theme_set(theme_bw(16))

        if not hasattr(plots, '__iter__'):
            plots = [plots]

        plots_array = []
        for plt in plots:
            try:
                plots_array.append(plt.plot())
            # pylint: disable=no-member
            except rpy2.rinterface.RRuntimeError as e:
                print(e)
        grdevices = importr('grDevices')
        grdevices.dev_off()
        return plots_array


class Report(object):
    """\
    Base class for reports.

    """
    def __init__(self, renderer):
        """\

        :param renderer: the renderer to use

        """
        self.renderer = renderer

    def generate(self, results):
        """\
        Generates the report from analysis results.

        :param results: analysis results
        :return:

        """
        raise NotImplementedError()


class CorrelationReport(Report):
    """
    Plots a field against outcome column and calculates the AUC value.
    """

    def get_auc_score(self, feature_name, problem):
        """
        :param feature_name: the feature whose auc with the outcome column should be calculated
        :return: a float representing the AUC value
        """
        return ROC.from_scores(problem.y, problem.dataframe[feature_name].values).auc

    def get_corr_density_plot(self, feature_label, feature_column, dataframe, outcome_column, positive_outcome):
        """

        :param feature_label: The real name of the feature. rpy will not accept names with underscores
        so this is being passed seperately from feature_column.
        :param feature_column: The column containing the feature data with a feature name that rpy does not object to.
        :param dataframe: The data frame containing the feature and the outcome columns.
        :param outcome_column: The columns contining the outcome feature
        :param positive_outcome: The value of the positive outcome
        :return:A ggplot density plot
        """
        gp_corr = ggplot2.ggplot(pandas2ri.py2ri(dataframe))
        gp_corr += ggplot2.aes_string(x=feature_column, color=outcome_column)
        gp_corr += ggplot2.geom_density()
        gp_corr += ggplot2.geom_rug(alpha=0.1)
        gp_corr += ggplot2.xlab(feature_label)
        auc_score = self.get_auc_score(feature_column, Problem(dataframe, [feature_column],
                                                               outcome_column, positive_outcome))
        gp_corr += ggplot2.ggtitle("{} vs. {} . AUC={:.3f}".format(feature_label, outcome_column, auc_score))
        return gp_corr

    def get_corr_box_plot(self, feature_label, feature_column, dataframe, outcome_column, positive_outcome):
        """

        :param feature_label: The real name of the feature. rpy will not accept names with underscores
        so this is being passed seperately from feature_column.
        :param feature_column: The column containing the feature data with a feature name that rpy does not object to.
        :param dataframe: The data frame containing the feature and the outcome columns.
        :param outcome_column: The columns contining the outcome feature
        :param positive_outcome: The value of the positive outcome
        :return: a ggplot2 boxplot with jittered points in the background
        """
        gp_corr = ggplot2.ggplot(pandas2ri.py2ri(dataframe))
        gp_corr += ggplot2.aes_string(x=outcome_column, y=feature_column, color=outcome_column)
        gp_corr += ggplot2.geom_rug(alpha=0.1)
        gp_corr += ggplot2.geom_boxplot(**{'outlier.shape': float('NaN')})
        gp_corr += ggplot2.geom_point(position="jitter")
        gp_corr += ggplot2.ylab(feature_label)
        auc_score = self.get_auc_score(feature_column, Problem(dataframe, [feature_column],
                                                               outcome_column, positive_outcome))
        gp_corr += ggplot2.ggtitle("{} vs. {} . AUC={:.3f}".format(feature_label, outcome_column, auc_score))
        return gp_corr

    def get_corr_point_plot(self, feature_label, feature_column, dataframe, outcome_column, positive_outcome):
        """

        :param feature_label: The real name of the feature. rpy will not accept names with underscores
        so this is being passed seperately from feature_column.
        :param feature_column: The column containing the feature data with a feature name that rpy does not object to.
        :param dataframe: The data frame containing the feature and the outcome columns.
        :param outcome_column: The columns contining the outcome feature
        :param positive_outcome: The value of the positive outcome
        :return: a jittered ggplot2 point plot

        """
        gp_corr = ggplot2.ggplot(pandas2ri.py2ri(dataframe))
        gp_corr += ggplot2.aes_string(x=feature_column, y=outcome_column, color=outcome_column)
        gp_corr += ggplot2.geom_point(position="jitter")
        gp_corr += ggplot2.geom_rug(alpha=0.1)
        gp_corr += ggplot2.ylab(feature_label)
        auc_score = self.get_auc_score(feature_column, Problem(dataframe, [feature_column],
                                                               outcome_column, positive_outcome))
        gp_corr += ggplot2.ggtitle("{} vs. {} . AUC={:.3f}".format(feature_label, outcome_column, auc_score))
        return gp_corr

    def generate(self, results):
        """\
        generates a pdf containing the plots and a text file containing a sorted list of AUV values.
        The temporary dataframe, temp_df, contains only two columnsa and is therefore faster to use as the entire
        dataset does not have to be converted to an R dataframe.

        """
        temp_problem = results.clone()
        plots = []
        auc_values_list = [self.get_auc_score(column, results) for column in results.features]
        auc_df = pd.DataFrame({'Fields': results.features, 'AUC Values': auc_values_list})
        auc_df = auc_df.sort_values('AUC Values')
        self.renderer.add_table("Fields by AUC", auc_df)

        for column in results.features:
            temp_problem.dataframe['randfeat'] = results.dataframe[column].values
            temp_df = pd.DataFrame(data=temp_problem.dataframe, columns=['randfeat', 'report_status'])
            plots.append(self.get_corr_density_plot(column, 'randfeat', temp_df, 'report_status', 'YES'))
            plots.append(self.get_corr_box_plot(column, 'randfeat', temp_df, 'report_status', 'YES'))
            plots.append(self.get_corr_point_plot(column, 'randfeat', temp_df, 'report_status', 'YES'))
        self.renderer.add_plot("variants against {}".format(results.outcome_column), plots)


class ClassificationReport(Report):
    """\
    Generates reports for cross-validated classifiers

    """
    def __init__(self, renderer, output_train_scores=False, label_list=None):
        """

        :param renderer: renderer to use
        :param output_train_scores: whether to output CV sample scores for the training samples. Default: False

        """
        super(ClassificationReport, self).__init__(renderer)
        self.output_train_scores = output_train_scores
        self.label_list = label_list

    def summarize_performance(self, cv_results):
        """\
        Summarizes classification metrics.

        :param cv_results: list of results from each cross validation split
        :return: DataFrame with performance numbers, and also a dataframe with row averages

        """
        perf_df = self.generate_performance_metrics_dataframe(cv_results)

        # Compute averages across CV splits
        metric_cols = [col for col in perf_df.columns if col.startswith('train_') or col.startswith('test_')]
        average_row = assert_rows_are_concordant(perf_df, ignore_columns=['cv_index', "best_choice"] + metric_cols)
        average_row['best_choice'] = perf_df.groupby("best_choice").count().sort_values("cv_index").index[-1] \
            if "best_choice" in perf_df.columns else "None"
        average_row.update({metric: perf_df[metric].mean() for metric in metric_cols})

        return perf_df, pd.DataFrame([average_row])

    def generate_performance_metrics_dataframe(self, cv_results):
        """\
        Returns a pandas dataframe containing performance metrics.
        Functionality refactored outside of summarize_performance so ComparativeLearningApproachReport
        can use it.

        :param cv_results: list of results from each cross validation split
        :return: DataFrame with performance numbers

        """
        perf_df = pd.DataFrame()
        perf_df['cv_index'] = [r['cv_index'] for r in cv_results]
        perf_df['approach_type'] = [r['approach']['type'] for r in cv_results]
        perf_df['best_choice'] = [r.get('best_choice', 'None') for r in cv_results]
        perf_df['n_features'] = [r['train']['n_features'] for r in cv_results]
        for metric in list(cv_results[0]['test']['metrics'].keys()):
            perf_df['train_{}'.format(metric)] = [r['train']['metrics'][metric] for r in cv_results]
            perf_df['test_{}'.format(metric)] = [r['test']['metrics'][metric] for r in cv_results]
        return perf_df

    def summarize_features(self, cv_results):
        """\
        Summarizes info about which features were selected in cross validation.

        :param cv_results: list of results from each cross validation split
        :return: DataFrame with feature statistics

        """
        def median_or_nan(lst):
            """Returns the median if list is non-empty, or nan otherwise"""
            return np.median(lst) if len(lst) > 0 else float('nan')

        feature_counts = Counter()
        feature_p_vals = defaultdict(list)
        for r in cv_results:
            feature_counts.update(r['approach'].get('selected_features', []))
            for feat, p_val in r['approach'].get('feature_p_values', {}).items():
                feature_p_vals[feat].append(p_val)

        df = pd.DataFrame(list(feature_counts.items()), columns=['feature', 'times_selected'])
        df['median_p_value'] = [median_or_nan(feature_p_vals.get(feat)) for feat in df['feature']]
        df['frequency'] = df['times_selected'] / len(cv_results)
        df['n_cv_splits'] = len(cv_results)
        return df.sort_values('median_p_value', ascending=True)

    def summarize_scores(self, cv_results):
        """\
        Summarizes sample scores.

        :param cv_results: list of results from each cross validation split
        :return: DataFrame with sample scores

        """
        scores_column_order = ['subset', 'sample', 'outcome', 'positive_outcome', 'truth', 'score']

        def cv_result_to_frame(cv_result):
            """Converts results from a single CV split into a DataFrame"""
            frames = []

            for subset in ['train', 'test']:
                y_score = cv_result[subset]['scores']
                y_truth = cv_result[subset]['truth']
                if len(y_score) > 0 and isinstance(y_score[0], np.ndarray):
                    if y_score[0].shape[0] != 2:
                        y_score = [y_score[index, y_truth[index]] for index in range(len(y_score))]
                    else:
                        # binary case with score of 2 computed for each sample with 1 being positive_outcome column
                        y_score = y_score[:, 1]
                sdf = pd.DataFrame({'sample': cv_result[subset]['sample'],
                                    'score': y_score,
                                    'truth': y_truth,
                                    'outcome': cv_result[subset]['outcome']})
                sdf['cv_index'] = cv_result['cv_index']
                sdf['positive_outcome'] = cv_result[subset]['positive_outcome']
                sdf['subset'] = subset
                frames.append(partially_reorder_columns(sdf, ['cv_index'] + scores_column_order))
            return pd.concat(frames, ignore_index=True)

        cv_scores_df = pd.concat([cv_result_to_frame(r) for r in cv_results], ignore_index=True)
        if not self.output_train_scores:
            cv_scores_df = cv_scores_df[cv_scores_df['subset'] != 'train']

        # Compute average scores across CV splits
        averages = []
        for _, sample_sdf in cv_scores_df.groupby(by=['sample', 'subset']):
            average_row = assert_rows_are_concordant(sample_sdf, ignore_columns=['cv_index', 'score'])
            average_row['score'] = sample_sdf['score'].mean()
            averages.append(average_row)

        return cv_scores_df, partially_reorder_columns(pd.DataFrame(averages), scores_column_order)

    def compute_with_averaging_for_multiclass(self, y_truth, score_truth, fcn):
        """ Computes value for binary cases or averaging for multiclass using upper bound for score in
             case of wrong prediction
        """
        result = 0.0
        unique_values = np.unique(y_truth)
        if len(unique_values) > 2:
            for class_value in unique_values:
                indicator_truth = [1 if y == class_value else 0 for y in y_truth]
                indicator_score_estimate = [score_truth[ind] if y_truth[ind] == class_value
                                            else 1.0 - score_truth[ind] for ind in range(len(y_truth))]
                result += fcn(indicator_truth, indicator_score_estimate)
            result /= len(unique_values)
        else:
            result = fcn(y_truth, score_truth)
        return result

    def get_score_plots(self, mean_scores_df):
        """\
        Generates score plots.

        :param mean_scores_df: DataFrame with mean sample scores
        :return: list of plots

        """

        rdf = pandas2ri.py2ri(mean_scores_df)

        if len(mean_scores_df['truth'].unique()) == 2:
            auc = ROC.from_scores(mean_scores_df['truth'], mean_scores_df['score']).auc_ci
        else:
            auc = self.compute_with_averaging_for_multiclass(mean_scores_df['truth'], mean_scores_df['score'],
                                                             roc_auc_score)

        gp_hist = ggplot2.ggplot(rdf)
        gp_hist += ggplot2.aes_string(x='score', color='factor(outcome)', fill='factor(outcome)')
        gp_hist += ggplot2.geom_histogram(color='black', position='dodge')
        gp_hist += ggplot2.geom_rug()
        gp_hist += ggplot2.facet_grid('subset ~ .')
        gp_hist += ggplot2.ggtitle('CV scores. AUC={}'.format(auc))

        gp_density = ggplot2.ggplot(rdf)
        gp_density += ggplot2.aes_string(x='score', color='factor(outcome)', fill='factor(outcome)')
        gp_density += ggplot2.geom_density(alpha=0.5)
        gp_density += ggplot2.geom_rug()
        gp_density += ggplot2.facet_grid('subset ~ .')
        gp_density += ggplot2.ggtitle('CV scores. AUC={}'.format(auc))

        gp_box = ggplot2.ggplot(rdf)
        gp_box += ggplot2.aes_string(x='factor(outcome)', y='score', color='factor(outcome)')
        gp_box += ggplot2.geom_boxplot(**{'outlier.shape': float('nan')})
        gp_box += ggplot2.geom_point(position='jitter', alpha=0.5)
        gp_box += ggplot2.facet_grid('subset ~ .')
        gp_box += ggplot2.ggtitle('CV scores. AUC={}'.format(auc))
        return [gp_box, gp_hist, gp_density]

    def get_roc_plot(self, mean_scores_df):
        """\
        Generates ROC plots.

        :param mean_scores_df: DataFrame with mean sample scores
        :return: ROC plot

        """
        unique_values = np.unique(mean_scores_df['truth'])
        if len(unique_values) > 2:
            return None
        roc = ROC.from_scores(mean_scores_df['truth'], mean_scores_df['score'])
        rdf = pandas2ri.py2ri(roc.dataframe)

        gp = ggplot2.ggplot(rdf)
        gp += ggplot2.aes_string(x='fpr', y='tpr')
        gp += ggplot2.geom_line(color='black')
        gp += ggplot2.geom_point(color='red')
        gp += ggplot2.ggtitle('Receiver Operating Characteristic\nAUC={}'.format(roc.auc_ci))
        gp += ggplot2.xlab("False Positive Rate")
        gp += ggplot2.ylab("True Positive Rate")
        return gp

    def generate(self, results):
        """\
        Generates the classification report.
        :param results: list of results from each cross validation split
        :return: None

        """
        cv_perf, mean_perf = self.summarize_performance(results)
        self.renderer.add_table('cv_metrics', cv_perf)
        self.renderer.add_table('mean_metrics', mean_perf)
        test_key = 'test'
        if len(results) > 0 and test_key in results[0] and 'confusion_matrix' in results[0][test_key]:
            best_accuracy = -1.0
            best_accuracy_index = -1
            for index, cv_result in enumerate(results):
                accuracy_at_index = accuracy_from_confusion_matrix(cv_result[test_key]['truth'],
                                                                   cv_result[test_key]['scores'],
                                                                   cv_result[test_key]['confusion_matrix'])
                if accuracy_at_index > best_accuracy:
                    best_accuracy = accuracy_at_index
                    best_accuracy_index = index
            if best_accuracy_index >= -1 and 'confusion_matrix' in results[best_accuracy_index][test_key]:
                cv_confusion_matrix = pd.DataFrame(results[best_accuracy_index][test_key]['confusion_matrix'])\
                    if self.label_list is None else\
                    pd.DataFrame(data=results[best_accuracy_index][test_key]['confusion_matrix'],
                                 columns=self.label_list)
                self.renderer.add_table('sample_confusion_matrix', cv_confusion_matrix)
                print(" best accuracy " + str(best_accuracy))

        cv_scores, mean_scores = self.summarize_scores(results)
        self.renderer.add_table('cv_sample_scores', cv_scores)
        self.renderer.add_table('mean_sample_scores', mean_scores)
        self.renderer.add_plot('score_plot', self.get_score_plots(mean_scores))

        self.renderer.add_table('selected_features', self.summarize_features(results))
        unique_values = np.unique(mean_scores['truth'])
        if len(unique_values) == 2:
            self.renderer.add_plot('roc', self.get_roc_plot(mean_scores))


class ComparativeClassificationReport(Report):
    """Report comparing learning approaches"""

    def get_concatenated_metrics(self, results):
        """\
        Generates the base concatenated report data from analysis results.

        :param results: Dictionary mapping LearningApproaches to results
        :return:

        """
        reporter = ClassificationReport(self.renderer)
        reports = []
        for approach in results:
            _, perf_df = reporter.summarize_performance(results[approach])
            perf_df["approach"] = str(approach)
            reports.append(perf_df)
        return pd.concat(reports)

    def get_concatenated_scores(self, results):
        """\
        Generates the concatenated scores from analysis results.

        :param results: Dictionary mapping LearningApproaches to results
        :return:

        """
        report = ClassificationReport(self.renderer)
        scores = []
        for approach in results:
            _, average_scores = report.summarize_scores(results[approach])
            average_scores["approach"] = str(approach)
            scores.append(average_scores)
        return pd.concat(scores)

    def get_score_plots(self, results):
        """\
        Generates the report from analysis results.

        :param results: Dictionary mapping LearningApproaches to results
        :return: list of plots ( [box, histogram, density] )

        """
        rdf = pandas2ri.py2ri(self.get_concatenated_scores(results))

        gp_hist = ggplot2.ggplot(rdf)
        gp_hist += ggplot2.aes_string(x='score', color='factor(outcome)', fill='factor(outcome)')
        gp_hist += ggplot2.geom_histogram(color='black', position='dodge')
        gp_hist += ggplot2.geom_rug()
        gp_hist += ggplot2.facet_grid('approach ~ .')

        gp_density = ggplot2.ggplot(rdf)
        gp_density += ggplot2.aes_string(x='score', color='factor(outcome)', fill='factor(outcome)')
        gp_density += ggplot2.geom_density(alpha=0.5)
        gp_density += ggplot2.geom_rug()
        gp_density += ggplot2.facet_grid('approach ~ .')

        gp_box = ggplot2.ggplot(rdf)
        gp_box += ggplot2.aes_string(x='factor(outcome)', y='score', color='factor(outcome)')
        gp_box += ggplot2.geom_boxplot(**{'outlier.shape': float('nan')})
        gp_box += ggplot2.geom_point(position='jitter', alpha=0.5)
        gp_box += ggplot2.facet_grid('approach ~ .')
        return [gp_box, gp_hist, gp_density]

    def generate(self, results):
        """\
        Generates the comparative report.
        :param cv_results: Dictionary mapping LearningApproaches to results (same input as get_score_plots)
        :return: None

        """
        self.renderer.add_table('mean_scores', self.get_concatenated_scores(results))
        self.renderer.add_table('mean_metrics', self.get_concatenated_metrics(results))
        self.renderer.add_plot('score_plots', self.get_score_plots(results))
